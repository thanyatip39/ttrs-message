FROM node:8.12.0-alpine As build-stage

WORKDIR /app

COPY ./src/package*.json /app/

RUN npm install

COPY ./src /app/

ARG configuration=production

RUN npm run build -- --output-path=./dist/out --configuration $configuration

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.15

COPY --from=build-stage /app/dist/out/ /usr/share/nginx/html

COPY ./config/nginx-custom.conf /etc/nginx/conf.d/default.conf
