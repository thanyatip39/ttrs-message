
import { Component, ViewChild, OnInit, OnDestroy, HostListener } from '@angular/core';
import { IcrmWsService, MessageData } from '../services/icrm-ws.service';
import { Observable, Subscription } from 'rxjs/';
import { MatTableDataSource, MatSort, MatPaginator, MatSortable} from '@angular/material';
import { AuthService } from '../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { TtrsApiService } from '../services/ttrs-api.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { environment } from 'environments/environment';
import { ReplyBoxComponent } from '../reply-box/reply-box.component';
import { NavbarComponent } from '../shared/navbar/navbar.component';
import { Howl } from 'howler'
declare const $: any;


@Component({
  selector: 'app-queues-cmp',
  templateUrl: './queues.component.html',
  styleUrls: ['./queues.component.css']
})
export class QueuesComponent implements OnInit, OnDestroy {
  @ViewChild(ReplyBoxComponent) replybox;
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(ReplyBoxComponent) replyBox: ReplyBoxComponent;
  @ViewChild(NavbarComponent) navNoti;
  private wsSubject: Observable<MessageData>;
  subscription: Subscription;
  private timelineData: Observable<MessageData>;
  private insertedId: string;
  private wsdata;
  private valueObject;
  displayName: string ;
  photoURL: string;
  role: string;
  private user;
  displayedColumns = ['Service', 'Time', 'Phonenumber', 'UserId', 'Magane'];
  private messageType = {0: ''};
  private messageTypeComplete = [];
  private messageTypeIncomplete = [];
  private editForm = false;
  private strTemp: any;
  private selectedRow;
  private nameUserSelectedRow = '';
  private jobStatus = [{id_complete: 1, text_complete: 'สนทนาสำเร็จ'}, {id_complete: 0, text_complete: 'สนทนาไม่สำเร็จ'}];
  private selectedStatus;
  private messageUnmanage = [];
  private messageRely;
  private medias = []
  inboxForm: FormGroup;
  private senddertime;
  private cleantime;
  private opentime;
  private initInbox = {};
  successReply = false;
  private haveNoti = false;
constructor(private http: HttpClient, private wsService: IcrmWsService, private auth: AuthService,
   private ttrsApi: TtrsApiService, private formBuilder: FormBuilder) {}

    ngOnInit() {
        // this.editForm = this.replyBox.checkChange;
        this.displayName = this.auth.getClaims()['nickname'];
        this.navNoti = this.displayName
        // this.displayName = 'Supatsorn'
        this.wsSubject = <Observable<MessageData>>this.wsService.getSubject();
        this.subscription = this.wsSubject.subscribe(res => {
        this.strTemp = res;
        if (res['event'] === 'err') {
          console.log('service close');
          // this.showNotification('top', 'right', 'danger');
        } else if (res['event'] === 'openService') {
          // this.showNotification('top', 'right', 'success');
        } else {
          this.wsdata  = JSON.parse(this.strTemp)
          switch (this.wsdata.event) {
            case 'noticeNewMessage':
              // this.showNotification(this.wsdata.data);
              break;
            case 'updateMessage':
              console.log(this.wsdata)
              this.dataSource = new MatTableDataSource(this.fiilterDateFormat(this.wsdata.data))
              this.dataSource.paginator = this.paginator;
              break;
            case 'newMessage':
              console.log(this.wsdata)
              this.dataSource = new MatTableDataSource(this.fiilterDateFormat(this.wsdata.data))
              this.dataSource.paginator = this.paginator;
              // this.showNotification(this.wsdata.data);
              break;
            case 'bookingJob':
            console.log(this.wsdata.data);
              this.dataSource = new MatTableDataSource(this.fiilterDateFormat(this.wsdata.data))
              this.dataSource.paginator = this.paginator;
              break;
            case 'closeJob':
              console.log(this.wsdata.data);
                this.dataSource = new MatTableDataSource(this.fiilterDateFormat(this.wsdata.data))
                this.dataSource.paginator = this.paginator;
                break;
            case 'timeline':
              if (this.wsdata.agent === this.displayName) {
                this.timelineData = this.fiilterDateFormat(this.wsdata.data);
                this.filterSender(this.timelineData);
              } else {
                this.editForm = false;
              }
              break;
            case 'reply':
              if (this.wsdata.status === 'success') {
                this.successReply = true;
              }
              break;
            default:
              console.log('Reciew wrong data!!');
              break;
          }
        }
      });

      this.ttrsApi.getMessageType().subscribe(type => {
        // this.messageType = type;
        console.log(this.messageType);
        type.map(data => {
          this.messageType[data['type_id']] = data['type_text'];
          // console.log(data['type_id']);
          if (data.type_complete === 1) {
              this.messageTypeComplete.push(data);
          } else if (data.type_complete === 0) {
            this.messageTypeIncomplete.push(data);
          }
        })
      });

      //move to top
      // $(window).scroll(function() {
      //   if ($(this).scrollTop()) {
      //       $('#toTop').fadeIn();
      //   } else {
      //       $('#toTop').fadeOut();
      //   }
      //   });
  }

  // scrollWin() {
  //   window.scrollTo(0, 0);
  // }

  ngOnDestroy() {
    console.log('onDestroy');
    this.subscription.unsubscribe()
    this.ngOnInit()
  }

  createForm() {
    this.inboxForm = this.formBuilder.group({
      messageTranslate : ['', ],
      completeReason : ['', Validators.required],
      selectedMessageType : ['', Validators.required],
      // messageReply : ['', Validators.required]
    })
  }

  seleteStatus (status) {
    this.selectedStatus = status;
  }

  fiilterDateFormat(data) {
    return data.map(row => {
      row.inputtime = row.inputtime.toString().replace('Z', '').replace('T', '\n').replace('.000', '');
      row.question = row.question === '0' ? 0 : row.question.split(',');
      return row;
    })
  }

  filterSender(data) {
    console.log(data);
    this.timelineData = data.map(row => {
      if (row.media_path !== null) {
        const medias = row.media_path;
        row.media_path = row.media_path.split('},').map(media => {
          return JSON.parse(media.replace(new RegExp('"/', 'g'), '\"' + environment.URL_GET_MEDIASFILE + '/')
                .replace(new RegExp('}', 'g'), '') + '}')
        })
      }
      if (row.message_read_unread_id === 0 &&  row.complete === 0) {
          row.own = 'selectedData';
      } else if (row.adduser === 'api.addsms' || row.adduser === 'api.addmobileapp') {
        row.own = 'user'
      } else {
        row.own = 'agent'
      }
      return row;
    });
    console.log(this.timelineData);
  }

  bookingJob(rowJob) {
    console.log('booking id: ', rowJob.messageid);
    this.ttrsApi.getNameUser(rowJob.identification_patient).subscribe(data => {
    this.nameUserSelectedRow = data.prefixname + ' ' + data.name + ' ' + data.lastname
    })
    this.messageRely = {
      messageid: rowJob.messageid,
      phonenumber: rowJob.phonenumber,
      identification: rowJob.identification_patient,
      agent: this.displayName,
    }
    // this.selectedRow = rowJob;
    this.editForm = true;
    this.wsService.send({
      event: 'booking',
      agent: this.displayName,
      messageid: rowJob.messageid
    });
    this.manageMessage(rowJob);
  }

  manageMessage(rowJob) {
    this.wsService.send({
      event: 'getTimeline',
      agent: this.displayName,
      phonenumber: rowJob.phonenumber
    });
    // this.replyMessage();
    console.log( this.timelineData);
  }

  showNotification(newMessage) {
    const countnoti = {'Emergency': 0, 'App': 0, 'SMS': 0}
    const iconNoti = {'Emergency': 'local_hospital', 'App': 'apps', 'SMS': 'email'}
    newMessage.forEach(row => {
      if (row['source'] === 8) {
        countnoti['Emergency'] += 1
      } else if (row['source'] === 6) {
        countnoti['App'] += 1
      } else if (row['source'] === 1) {
        countnoti['SMS'] += 1
      }
    })
    console.log(countnoti)
    const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];
    let color;
    for (const key in countnoti) {
      if (countnoti.hasOwnProperty(key)) {
        if (key === 'Emergency') {
         color = type.indexOf('danger')
        } else  if (key === 'App') {
          color = type.indexOf('info')
        } else  if (key === 'SMS') {
          color = type.indexOf('rose')
        }

        if (countnoti[key] !== 0) {
          $.notify({
            icon: iconNoti[key],
            message: 'มีข้อความจาก ' + key + ' ' + countnoti[key] + ' ข้อความ'
          }, {
            type: type[color],
            timer: 3000,
            placement: {
                from: 'bottom',
                align: 'right'
            },
            template: '<div data-notify="container" class="col-xs-11 col-sm-4 alert alert-{0} alert-with-icon" role="alert">' +
              '<button mat-raised-button type="button" aria-hidden="true" class="close" data-notify="dismiss">' +
              '<i class="material-icons">close</i></button>' +
              '<i class="material-icons" data-notify="icon">' + iconNoti[key] + '</i> ' +
              '<span data-notify="title">{1}</span> ' +
              '<span data-notify="message">{2}</span>' +
              '<div class="progress" data-notify="progressbar">' +
              '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0"' +
              'aria-valuemax="100" style="width: 0%;"></div>' +
              '</div>' +
              '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
        // this.haveNoti = false;
        }
      }
  }
  }

  closeJob(messageid) {
    console.log('close Job', messageid);
    const closemessage = {
      event: 'closeJob',
      messageid: messageid,
      message_translatetext: this.inboxForm.get('messageTranslate').value,
      cleanuser: this.displayName,
      complete: this.inboxForm.get('selectedMessageType').value,
      message_type: this.inboxForm.get('completeReason').value,
    }
    this.timelineData.map(data => {
      if (data['messageid'] === messageid) {
        data['own'] = 'user';
      }
    })
    this.wsService.send(closemessage);
    this.clearForm()
    this.editForm = false;
  }

  clearForm() {
    this.inboxForm.get('selectedMessageType').setValue('');
    this.inboxForm.get('completeReason').setValue('');
    this.inboxForm.get('messageTranslate').setValue('');
  }

  // @HostListener('window:scroll', ['$event'])
  // onWindowScroll(e) {
  //    if (window.pageYOffset > 50) {
  //      const element = document.getElementById('myHeader');
  //      element.classList.add('sticky');
  //    } else {
  //     const element = document.getElementById('myHeader');
  //       element.classList.remove('sticky');
  //    }
  // }
}

