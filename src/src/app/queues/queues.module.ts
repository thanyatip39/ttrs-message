import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { QueuesRoutes } from './queues.routing';
import { QueuesComponent } from './queues.component';
import {  ReplyBoxModule } from '../reply-box/reply-box.module';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatExpansionModule} from '@angular/material/expansion';

@NgModule({
  imports: [
    CommonModule,
    ReplyBoxModule,
    RouterModule.forChild(QueuesRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MatTabsModule,
    MatTableModule,
    MatExpansionModule
  ],
  declarations: [QueuesComponent]
})
export class QueuesModule { }
