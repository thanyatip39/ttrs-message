import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReplyBoxComponent } from './reply-box.component';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ngfModule, ngf } from 'angular-file';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppComponent } from '../app.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatExpansionModule} from '@angular/material/expansion';

@NgModule({
    imports: [ RouterModule,
        MaterialModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ngfModule,
        MatFormFieldModule,
        MatExpansionModule
    ],
    declarations: [ ReplyBoxComponent ],
    exports: [ ReplyBoxComponent ]
})

export class ReplyBoxModule {}
platformBrowserDynamic().bootstrapModule(ReplyBoxModule);
