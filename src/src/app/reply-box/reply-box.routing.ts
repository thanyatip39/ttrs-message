import { Routes } from '@angular/router';

import { ReplyBoxComponent } from './reply-box.component';

export const RelyBoxRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: ReplyBoxComponent
    }]
}
];
