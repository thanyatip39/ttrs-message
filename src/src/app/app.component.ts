import { Component, OnInit } from '@angular/core';
import { OAuthService, AuthConfig, NullValidationHandler } from 'angular-oauth2-oidc';
import { authConfig } from './auth.config';
import { Router } from '@angular/router';
import { filter, delay } from 'rxjs/operators';
@Component({
    selector: 'app-my-app',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {

    constructor(private oauthService: OAuthService) {
    }

    ngOnInit() {
    }
}
