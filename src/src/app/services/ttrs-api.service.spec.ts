import { TestBed, inject } from '@angular/core/testing';

import { TtrsApiService } from './ttrs-api.service';

describe('TtrsServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TtrsApiService]
    });
  });

  it('should be created', inject([TtrsApiService], (service: TtrsApiService) => {
    expect(service).toBeTruthy();
  }));
});
