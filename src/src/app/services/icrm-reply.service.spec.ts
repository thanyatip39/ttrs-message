import { TestBed, inject } from '@angular/core/testing';

import { IcrmReplyService } from './icrm-reply.service';

describe('IcrmReplyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IcrmReplyService]
    });
  });

  it('should be created', inject([IcrmReplyService], (service: IcrmReplyService) => {
    expect(service).toBeTruthy();
  }));
});
