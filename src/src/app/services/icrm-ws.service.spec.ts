import { TestBed, inject } from '@angular/core/testing';

import { IcrmWsService } from './icrm-ws.service';

describe('IcrmApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IcrmWsService]
    });
  });

  it('should be created', inject([IcrmWsService], (service: IcrmWsService) => {
    expect(service).toBeTruthy();
  }));
});
