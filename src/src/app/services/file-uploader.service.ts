import { Injectable } from '@angular/core'
import { HttpClient, HttpRequest, HttpResponse, HttpEvent } from '@angular/common/http'
import { Subscription } from 'rxjs/';
import { environment } from 'environments/environment';
@Injectable()
export class FileUploaderService {
  postUrl = environment.URL_REPLY_MESSAGE;
  httpEvent: HttpEvent<{}>
  tempEvent: HttpEvent<{}>
  accept = '*'
  files: File[] = [];
  progress: number;
  hasBaseDropZoneOver = false;
  httpEmitter: Subscription;
  lastFileAt: Date;
  sendableFormData = FormData;

  constructor(public httpClient: HttpClient) {}

  uploadFiles(files: File[], fileFormdata: FormData): Subscription {
    // fileFormdata.append('medias', files[0], files['name'])
    fileFormdata.append('phonenumber', '089111111');
    fileFormdata.append('identification_patient', '3105012825261');
    fileFormdata.append('message_orgtext', 'test 12345678');
    fileFormdata.append('adduser', 'agent2');
    fileFormdata.append('message_read_unread_id', '0');
    fileFormdata.append('massage_view', '1');
    fileFormdata.append('reference_messageid', '20');
    // console.log('form data variable :   ' + fileFormdata.get('phonenumber'));

    const req = new HttpRequest<FormData>('POST', this.postUrl, fileFormdata, {
      reportProgress: true
    })
    return this.httpEmitter = this.httpClient.request(req)
    .subscribe(
      event => {
        this.httpEvent = event
        if (event instanceof HttpResponse) {
          delete this.httpEmitter
          console.log('request done', event)
        }
      },
      error => console.log('Error Uploading', error)
    )
  }

  cancel() {
    this.progress = 0
    if ( this.httpEmitter ) {
      console.log('cancelled')
      this.httpEmitter.unsubscribe()
    }
  }

}
