import { Component, OnInit, Renderer, ViewChild, ElementRef, Directive, Input } from '@angular/core';
import { ROUTES } from '../../sidebar/sidebar.component';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { IcrmWsService, MessageData } from '../../services/icrm-ws.service';
import { Observable, Subscription } from 'rxjs/';
import { Howl } from 'howler';

declare const $: any;
const misc: any = {
    navbar_menu_visible: 0,
    active_collapse: true,
    disabled_collapse_init: 0,
};

@Component({
    selector: 'app-navbar-cmp',
    templateUrl: 'navbar.component.html',
    styleUrls: ['./navbar.component.css'],
})

export class NavbarComponent implements OnInit {
    private listTitles: any[];
    location: Location;
    mobile_menu_visible: any = 0;
    private nativeElement: Node;
    private toggleButton: any;
    private sidebarVisible: boolean;
    private _router: Subscription;
    private countNotice: number;
    private wsSubject: Observable<MessageData>;
    private subscription: Subscription;
    private wsdata;
    private strTemp: any;
    private countUnmanage = { 'Emergency': 0, 'App': 0, 'SMS': 0 };

    @Input() notiData;
    @ViewChild('app-navbar-cmp') button: any;

    constructor(private wsService: IcrmWsService, location: Location, private renderer: Renderer,
        private element: ElementRef, private router: Router) {
        this.countNotice = 0;
        console.log('data binded: ', this.notiData)
        this.location = location;
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }
    minimizeSidebar() {
        const body = document.getElementsByTagName('body')[0];

        if (misc.sidebar_mini_active === true) {
            body.classList.remove('sidebar-mini');
            misc.sidebar_mini_active = false;

        } else {
            setTimeout(function () {
                body.classList.add('sidebar-mini');

                misc.sidebar_mini_active = true;
            }, 300);
        }

        // we simulate the window Resize so the charts will get updated in realtime.
        const simulateWindowResize = setInterval(function () {
            window.dispatchEvent(new Event('resize'));
        }, 180);

        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function () {
            clearInterval(simulateWindowResize);
        }, 1000);
    }
    hideSidebar() {
        const body = document.getElementsByTagName('body')[0];
        const sidebar = document.getElementsByClassName('sidebar')[0];

        if (misc.hide_sidebar_active === true) {
            setTimeout(function () {
                body.classList.remove('hide-sidebar');
                misc.hide_sidebar_active = false;
            }, 300);
            setTimeout(function () {
                sidebar.classList.remove('animation');
            }, 600);
            sidebar.classList.add('animation');

        } else {
            setTimeout(function () {
                body.classList.add('hide-sidebar');
                // $('.sidebar').addClass('animation');
                misc.hide_sidebar_active = true;
            }, 300);
        }

        // we simulate the window Resize so the charts will get updated in realtime.
        const simulateWindowResize = setInterval(function () {
            window.dispatchEvent(new Event('resize'));
        }, 180);

        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function () {
            clearInterval(simulateWindowResize);
        }, 1000);
    }

    ngOnInit() {
        const sound = new Howl({
            src: ['../../../assets/audio/notice.mp3']
        });
        this.wsSubject = <Observable<MessageData>>this.wsService.getSubject();
        this.subscription = this.wsSubject.subscribe(res => {
            this.strTemp = res;
            if (res['event'] === 'err') {
                console.log('service close');
                // this.showNotification('top', 'right', 'danger');
            } else if (res['event'] === 'openService') {
                // this.showNotification('top', 'right', 'success');
            } else {
                this.wsdata = JSON.parse(this.strTemp)
                switch (this.wsdata.event) {
                    case 'noticeNewMessage':
                        this.showNotification(this.wsdata.data);
                        sound.play();
                        break;
                    case 'updateMessage':
                        this.countUnmanageMessage(this.wsdata.data);
                        break;
                    case 'newMessage':
                        console.log('newMessage');
                        this.countUnmanageMessage(this.wsdata.data);
                        break;
                    default:
                        console.log('Reciew wrong data!!');
                        break;
                }
            }
        });


        this.listTitles = ROUTES.filter(listTitle => listTitle);

        const navbar: HTMLElement = this.element.nativeElement;
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        if (body.classList.contains('sidebar-mini')) {
            misc.sidebar_mini_active = true;
        }
        if (body.classList.contains('hide-sidebar')) {
            misc.hide_sidebar_active = true;
        }
        this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
            const $layer = document.getElementsByClassName('close-layer')[0];
            if ($layer) {
                $layer.remove();
            }
        });
    }

    countUnmanageMessage(newMessage) {
        const countnoti = { 'Emergency': 0, 'App': 0, 'SMS': 0 }
        const iconNoti = { 'Emergency': 'local_hospital', 'App': 'apps', 'SMS': 'email' }
        newMessage.forEach(row => {
            if (row['source'] === 8) {
                countnoti['Emergency'] += 1
            } else if (row['source'] === 6) {
                countnoti['App'] += 1
            } else if (row['source'] === 1) {
                countnoti['SMS'] += 1
            }
        })
       this.countUnmanage = countnoti;
    }

    showNotification(newMessage) {
        const countnoti = { 'Emergency': 0, 'App': 0, 'SMS': 0 }
        const iconNoti = { 'Emergency': 'local_hospital', 'App': 'apps', 'SMS': 'email' }
        newMessage.forEach(row => {
            if (row['source'] === 8) {
                countnoti['Emergency'] += 1
            } else if (row['source'] === 6) {
                countnoti['App'] += 1
            } else if (row['source'] === 1) {
                countnoti['SMS'] += 1
            }
        })
        console.log(countnoti)
        const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];
        let color;
        for (const key in countnoti) {
            if (countnoti.hasOwnProperty(key)) {
                if (key === 'Emergency') {
                    color = type.indexOf('danger')
                } else if (key === 'App') {
                    color = type.indexOf('info')
                } else if (key === 'SMS') {
                    color = type.indexOf('rose')
                }

                if (countnoti[key] !== 0) {
                    $.notify({
                        icon: iconNoti[key],
                        message: 'มีข้อความจาก ' + key + ' ' + countnoti[key] + ' ข้อความ'
                    }, {
                            type: type[color],
                            timer: 3000,
                            placement: {
                                from: 'bottom',
                                align: 'right'
                            },
                            template: '<div data-notify="container" class="col-xs-11 col-sm-4 alert alert-{0} ' +
                                'alert-with-icon" role="alert">' +
                                '<button mat-raised-button type="button" aria-hidden="true" class="close" data-notify="dismiss">' +
                                '<i class="material-icons">close</i></button>' +
                                '<i class="material-icons" data-notify="icon">' + iconNoti[key] + '</i> ' +
                                '<span data-notify="title">{1}</span> ' +
                                '<span data-notify="message">{2}</span>' +
                                '<div class="progress" data-notify="progressbar">' +
                                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0"' +
                                'aria-valuemax="100" style="width: 0%;"></div>' +
                                '</div>' +
                                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                                '</div>'
                        });
                    // this.haveNoti = false;
                }
            }
        }
    }

    onResize(event) {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    sidebarToggle() {
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
        const body = document.getElementsByTagName('body')[0];

        var $toggle = document.getElementsByClassName('navbar-toggler')[0];
        if (this.mobile_menu_visible === 1) {
            // $('html').removeClass('nav-open');
            body.classList.remove('nav-open');
            if ($layer) {
                $layer.remove();
            }

            setTimeout(function () {
                $toggle.classList.remove('toggled');
            }, 400);

            this.mobile_menu_visible = 0;
        } else {
            setTimeout(function () {
                $toggle.classList.add('toggled');
            }, 430);

            var $layer = document.createElement('div');
            $layer.setAttribute('class', 'close-layer');


            if (body.querySelectorAll('.main-panel')) {
                document.getElementsByClassName('main-panel')[0].appendChild($layer);
            } else if (body.classList.contains('off-canvas-sidebar')) {
                document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
            }

            setTimeout(function () {
                $layer.classList.add('visible');
            }, 100);

            $layer.onclick = function () { //asign a function
                body.classList.remove('nav-open');
                this.mobile_menu_visible = 0;
                $layer.classList.remove('visible');
                setTimeout(function () {
                    $layer.remove();
                    console.log('aici')
                    $toggle.classList.remove('toggled');
                }, 400);
            }.bind(this);

            body.classList.add('nav-open');
            this.mobile_menu_visible = 1;

        }
    }

    getTitle() {
        let titlee: any = this.location.prepareExternalUrl(this.location.path());
        for (let i = 0; i < this.listTitles.length; i++) {
            if (this.listTitles[i].type === "link" && this.listTitles[i].path === titlee) {
                return this.listTitles[i].title;
            } else if (this.listTitles[i].type === "sub") {
                for (let j = 0; j < this.listTitles[i].children.length; j++) {
                    let subtitle = this.listTitles[i].path + '/' + this.listTitles[i].children[j].path;
                    if (subtitle === titlee) {
                        return this.listTitles[i].children[j].title;
                    }
                }
            }
        }
        return 'Dashboard';
    }
    getPath() {
        return this.location.prepareExternalUrl(this.location.path());
    }
}
