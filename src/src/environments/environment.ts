// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  URL_GET_MESSAGETYPE: 'http://localhost:4000/messageType/ttrsMessage',
  URL_GET_TYPECOMPLETE: 'http://localhost:4000/typeComplete/textchat',
  URL_GET_NAMEUSER: 'http://localhost:4000/nameUser/',
  URL_GET_MEDIASFILE: 'http://localhost:3000/file',
  URL_REPLY_MESSAGE: 'http://localhost:3000/messageReply',
  WS_SERVER: 'ws://localhost:3000/wsTrigger/'
};
