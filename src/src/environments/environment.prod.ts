export const environment = {
  production: true,
  URL_GET_MESSAGETYPE: 'http://localhost:4000/messageType/ttrsMessage',
  URL_GET_TYPECOMPLETE: 'http://localhost:4000/typeComplete/textchat',
  URL_GET_NAMEUSER: 'http://localhost:4000/nameUser/',
  URL_GET_MEDIASFILE: 'http://localhost:3000/file',
  URL_REPLY_MESSAGE: 'http://localhost:3000/messageReply',
  WS_SERVER: 'ws://localhost:3000/wsTrigger/'
};
